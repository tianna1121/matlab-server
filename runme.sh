#!/bin/sh
export BBENCH=1
export SINGLE=0
export DUAL=1
export QUAL=0

############# bbench ########################
if [ $BBENCH -eq 1 ]; then
if [ $SINGLE -eq 1 ]; then
echo "BBENCH singlecore.a15"
tar xvf ../m5out.bbench.single.a15.tgz
cd m5out/
cp ../m5stats2streamline.py .
cp ../o3_config.ini .
./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
mv newStats.txt ../newStats-singleCore-a15-bbench.txt
cd ..
rm -rf m5out/
fi

if [ $DUAL -eq 1 ]; then
echo "BBENCH dualcore.a15"
tar xvf ../m5out.bbench.dual.a15.tgz
cd m5out/
cp ../m5stats2streamline.py .
cp ../o3_config.ini .
./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
mv newStats.txt ../newStats-dualCore-a15-bbench.txt
cd ..
rm -rf m5out/
fi

if [ $QUAL -eq 1 ]; then
echo "BBENCH qualcore.a15"
tar xvf ../m5out.bbench.qual.a15.tgz
cd m5out/
cp ../m5stats2streamline.py .
cp ../o3_config.ini .
./m5stats2streamline.py ./o3_config.ini ./ single-a15.apc
mv newStats.txt ../newStats-qualCore-a15-bbench.txt
cd ..
rm -rf m5out/
fi
fi
