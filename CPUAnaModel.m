% 入口参数：
% 1. 用于分析和评估的原始数据，格式为singleCore-default-bbench
% 2. 用于分析和评估的模型名称，可选：TOCS09, ISPASS11, mech+ANN, pureANN等
% 3. 计算稳定状态CPI构成中是否选择加权动态指令数目，可选：weighted或者NotWeighted
% 4. 被分析线程最少执行次数，该数值越大，则被分析的线程数目越少，可设为1000-1700之间
% 5. 结果中需要采样点起始位置
% 6. 结果中需要采样点的数目，一般设置为100-300之间
% 示例1：CPUAnaModel('singleCore-a15-bbench', 'TOCS09', 'weighted', 1700, 200)
% 示例2：CPUAnaModel('singleCore-a15-bbench', 'paper', 'weighted', 1700, 200, 30)
% 2014-01-17 v1.0: zhangyang, startup
% 2014-02-21 v2.0: zhangyang, combine several configurations
% 2014-04-06 v3.0: zhangyang, 增加架构相关的PCA分析部分,目前只针对单核参数
% 2014-06-28 v4.0: zhangyang, 按照ISPASS‘11论文对分支确定时间cdr、MLP和结构冲突进行非线性回归拟合
% 2014-09-01 v4.1: zhangyang, 按照小论文增加结果输入
% 2015-01-05 v4.2: zhangyang, change the parameters

function simuTime = CPUAnaModel(FileName, ModelName, instNums, exeNums, simpoints, points)
t1=clock;
format long;
% PreFix = '/Users/yangzhang/ws/research/gem5/matlab/newStats/';
% PreFix = '/home/yangzhang/project/gem5-cases/gem5/android-single-baidumap/matlab/';
PreFix = pwd;
FilePath= strcat(PreFix, '/newStats-', FileName, '.mat');
if (exist(FilePath, 'file'))
    load(FilePath);
else
    strcat(PreFix, '/newStats-', FileName, '.txt')
    rawData = importdata(strcat(PreFix, '/newStats-', FileName, '.txt'));
    save(FilePath, 'rawData');
end

[~, colRawData] = size(rawData); % 获得加载数据数组矩阵的大小
sortedData = sortrows(rawData, [colRawData-2, colRawData]); % 按照PID号排序
[~, I] = unique(sortedData(:, colRawData-2)); % 查找PID号对应的行号
[rowI, ~] = size(I);
% 定义参数矩阵
pFEDp = 6; % 流水线前端深度（含延时）
pFEWd = 3; % 流水线前端宽度
pCDR = 5; MLP = 3; avgMem = 400;  % 估计参数
pAdjust = 8;
paramMatC = { ...
    'L1ICacheHit', 1; 
    'L1ICMiss', 12;
    'L1DCacheHit',         2;
    'L1DCacheMiss',        12;
    'CommittedInsts',      0;
    'CommitSquashedInsts', 0;
    'NumCycles',           0;
    'IdleCycles',          0;
    'IPC_Total',           0;
    'BrMiss',              1;
    'ITLBHit',             0;
    'ITLBMiss',            1;
    'DTLBHit',             1;
    'DTLBMiss',            1;
    'CommitLoads',         1;
    'CommitMembars',       1;
    'CommitBranches',      1;
    'CommitFPInst',        1;
    'CommitIntInsts',      1;
    'IntRFReads',          1;
    'IntRFWrites',         1;
    'MISCRFReads',         1;
    'MISCRFWrites',        1;
    'RenameROBFull',       1;
    'IQFull',              1;
    'LSQFull',             1;
    };

pMatMicro = [ ...
    1, 12, 2, 12, ... % L1ICacheHit, L1ICacheMiss, L1DCacheHit, L1DCacheMiss-4
    1, 1, 1, 1, 1, ... %CommittedInst, CommitSquashedInsts, NumCycles, IdleCycles, IPC_Total-9
    1, ... % BrMiss-10
    1, 12, 1, 12, ... % ITLBHit, ITLBMiss, DTLBHit, DTLBMiss-14
    1, 1, 1, 1, 1, ... % CommitLoads, CommitMembars, CommitBranches, CommitFPInst, CommitIntInsts-19
    1, 1, 1, 1, ... %IntRFReads, IntRFWrites, MiscRFReads, MiscRFWrites-23
    1, 1, 1, ... % RenameROBFull, IQFull, LSQFull-26
    ];
[~, colM] = size(pMatMicro);

pMI = [ ...
    1, 1, 3, 12, ...  % No_OpClass, IntAlu, IntMult, IntDiv-4
    5, 5, 5, 4, 9, 33, ... % FloatAdd, FloatCmp, FloatCvt, FloatDiv, FloatSqrt-10
    4, 4, 4, 4, 3, 3, ...  % SimdAdd, SimdAddAcc, SimdAlu, SimdCmp, SimdCvt, SimdMisc-16
    5, 5, 3, 3, 9, ... % SimdMult, SimdMultAcc, SimdShift, SimdShiftAcc, SimdSqrt-21
    5, 5, 3, 3, 3, ... % SimdFloatAdd, SimdFloatAlu, SimdFloatCmp, SimdFloatCvt, SimdFloatDiv-26
    3, 3, 3, 9, ... % SimdFloatMisc, SimdFloatMult, SimdFloatMultAcc, SimdFloatSqrt-30
    2, 2, 3, 1, 1, ... % MemRead, MemWrite, IprAccess, InstPrefetch, serialCycle-35
    ];
[~, colI] = size(pMI);

pMatSys = [ ...
    1, 1, 1, 1, ... % L2CacheHit, L2CacheMiss, L2CacheMissInst, L2CacheMissData-4
    1, 1, 1, ... % L2CacheMSHRMissInst, L2CacheMSHRMissPre, L2CacheMSHRMissData-7
    1, 1, ... % L2CacheMissInstLatency, L2CacheMissDataLatency-9
    1, 1, ... % PHYMEMRead, PHYMEMWrite-11
    ];

pMat = [pMatMicro, pMI, pMatSys];

% 将不同PID的线程数据加入sMats.pidX数据结构中
for i = 1 : (rowI-1)
    if (I(i+1)-I(i) > exeNums)
        sMats.i = sortedData(I(i):I(i+1), :);
        iNum = sum(sMats.i(:, colM+1:colM+colI),2);
        iNumFP = sum(sMats.i(:, colM+5:colM+10),2);
        %iNumSIMD = sum(sMats.i(:, colM+11:colM+30),2);
        switch instNums
            case 'weighted'
                sMats.i(:,end+1)=(sum(sMats.i(:,colM+1:colM+colI)*pMI',2))/pFEWd; %Base1:加权指令/宽度
            case 'notWeighted'
                sMats.i(:,end+1)=iNum/pFEWd;               % Base1部分:指令数/宽度
            otherwise
                fprintf('Not supported!\n');
        end
        
        sMats.i(:,end+1) = sMats.i(:,2) .* pMat(2);    % L1ICache缺失开销
        sMats.i(:,end+1) = sMats.i(:, 12) .* pMat(12); % L1ITLB缺失
        sMats.i(:,end+1) = sMats.i(:, 14) .* pMat(14); % D1ITLB缺失
        
        switch ModelName
            case 'PCA'
                figure;
                boxplot(sMats.i(:,1:24),...
                    'orientation','horizontal','labels', paramMatC(1:24,1));
                grid on;
                [rowSel, ~] = size(sMats.i(:,1:24));
                stdr = std(sMats.i(:,1:24));
                sr = sMats.i(:,1:24)./repmat(stdr, rowSel, 1);
                [coef,score,latent,t2] = princomp(sr);
                % t2:329*1
                % 多元统计距离,记录的是每一个观察量到中心的距离
                % 通过latent,可以知道提取前几个主成分就可以了.
                figure;
                percent_explained = 100*latent/sum(latent);
                pareto(percent_explained);
                xlabel('Principal Component');
                ylabel('Variance Explained (%)');
            case 'TOCS09'
                sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMats.i(:,2)) + ...
                    sMats.i(:, (colM+colI+3)) + sMats.i(:,10) + ...
                    sMats.i(:, (colM+colI+2))/MLP);                     % Base2：dispatch低效因子
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem/MLP; % L2ICache
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem/MLP; % L2DCache
                sMats.i(:,end+1) = sMats.i(:, 10).*(pCDR+pFEDp);        % 分支预测失败开销
                sMats.i(:,end+1) = sMats.i(:,8);                        % idle Cycle数目
                sMats.i(:,end+1) = sMats.i(:,colM+35);                  % serial Cycle数目
                sMats.i(:,end+1) = sum(sMats.i(:,24:26),2);             % 冲突开销
                sMats.i(:,end+1) = sum(sMats.i(:,end-10:end),2);        % 总的Cycle数目
                
                adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust); % 去掉cycle数目过大的点
                %figure(); plot(sMats.i(adjust,end)./iNum(adjust,:)); hold on; plot(sMats.i(adjust,7)./iNum(adjust,:),'r');
                simu_CPI = sMats.i(1:simpoints,7)./iNum(1:simpoints,:);
                TOCS09_CPI = sMats.i(1:simpoints,end)./iNum(1:simpoints,:);
                bar3([simu_CPI, TOCS09_CPI], .8, 'detached');
                colormap([1 0 0;0 1 0;0 0 1]);
                %figure(); bar(sMats.i(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10), 'stacked');
                temp_results=[sMats.i(simpoints:simpoints+points,end-10:end-5), sMats.i(simpoints:simpoints+points,end-3:end-1)];
                figure(); bar(temp_results ./repmat(iNum(simpoints:simpoints+points,:),1,9), 'stacked');
                legend('Base','L1ITLB', 'L1ICache','L1DTLB','StructHazards','L2ICache','L2DCache', ...
                    'MissPredict','SerialInsts','Conflicts');
                csvwrite('test.csv', sMats.i(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10));
                corrCoef = corrcoef(sMats.i(adjust,end), sMats.i(adjust,7))
            case 'ISPASS11'
                sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMats.i(:,2)) + ...
                    sMats.i(:, (colM+colI+3)) + sMats.i(:,10) + ...
                    sMats.i(:, (colM+colI+2))/MLP);                     % Base2：dispatch低效因子
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem/MLP; % L2ICache
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem/MLP; % L2DCache
                sMats.i(:,end+1) = sMats.i(:, 10).*(pCDR+pFEDp);        % 分支预测失败开销
                sMats.i(:,end+1) = sMats.i(:,8);                        % idle Cycle数目
                sMats.i(:,end+1) = sMats.i(:,colM+35);                  % serial Cycle数目
                sMats.i(:,end+1) = sum(sMats.i(:,24:26),2)              % 冲突开销
                sMats.i(:,end+1) = sum(sMats.i(:,end-10:end),2);        % 总的Cycle数目
                
                adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust); % 去掉cycle数目过大的点
				TOCS09_CPI = sMats.i(1:simpoints,end)./iNum(1:simpoints,:);
				simu_CPI = sMats.i(1:simpoints,7)./iNum(1:simpoints,:);
				
                % pCDR = b1 * (max(40, 1/mpubr)).^b2 * (1+b3*fp) * (1+b4*mpuDL1)
                pCDR = ['b(1).*(max(40,1/(sMats.i(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*' ...
                    '(1+b(4).*sMats.i(:,colM+colI+1)./iNum)'];
                % MLP = b5 * (mpuDL2).^b6 * (mpuDTLB).^b7
                MLP = '(b(5).*((sMats.i(:,colM+colI+4)./iNum).^b(6)).*((sMats.i(:,14)./iNum).^b(7)))';
                % cStall = max(0, 1-cMiss./(iNum/D)+b8*(1+b9*fp)*(1+b10*mpuDL1))
                cM = '100';
                cStall_t = '(b(8).*(1+b(9).*iNumFP./iNum).*(1+b(10).*(sMats.i(:,colM+colI+1)./iNum)))';
                cStall =strcat('(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')');
                
                if(~exist('ispass11-b.mat','file'))
                	% 为了避免多个文件，我这里用匿名的函数指针方式做拟合
               		beta0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]'; % 初始化拟合权重
                	pDispatch=['(pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,(colM+colI+3))'...
                    	'+sMats.i(:,10)+sMats.i(:, (colM+colI+2))./']; % MLP); % dispatch低效因子
                	pL2I = 'sMats.i(:,(colM+colI+3)).*avgMem./'; % MLP;
                	pL2D = 'sMats.i(:,(colM+colI+4)).*avgMem./'; % MLP;
               		pBr = 'sMats.i(:,10).*(pFEDp+'; %pCDR);
               		eval(['[b,r,j,covb,mse] = nlinfit(sMats.i,sMats.i(:,7), @(b,m)', ...
                   		pDispatch,MLP,')+',pL2I,MLP,'+',pL2D,MLP,'+',pBr,pCDR,'+',cStall,')',',beta0);']);
               		b; % 显示拟合得到的b1, b2, b3, b4, b5, b6以及b7参数
					save('ispass11-b.mat', 'b');
				else
					load('ispass11-b.mat', 'b');
				end
                % 利用拟合出来的参数，重新计算得到CPI的值
                MLP = b(5).*((sMats.i(:,colM+colI+4)./iNum).^b(6)).*((sMats.i(:,14)./iNum).^b(7));
                pCDR = b(1).*(max(40,1/(sMats.i(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)* ...
                    (1+b(4).*sMats.i(:,colM+colI+1)./iNum);
                
                sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,  ...
                    (colM+colI+3))+sMats.i(:,10)+sMats.i(:, (colM+colI+2))./MLP); % dispatch低效因子
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem./MLP;          % L2ICache
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem./MLP;          % L2DCache
                sMats.i(:,end+1) = sMats.i(:,8);						          % idle Cycle数目
                sMats.i(:,end+1) = sMats.i(:,colM+35);							  % serial Cycle数目
                sMats.i(:,end+1) = sMats.i(:,10) .* (pCDR+pFEDp);                 % 分支预测失败开销
                sMats.i(:,end+1) = eval(['(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')']);
                sMats.i(:,end+1) = sum(sMats.i(:,end-10:end), 2); 				  % 总的Cycle数目
                
                adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust); 		  % 去掉cycle数目过大的点
				
				ISPASS11_CPI = sMats.i(1:simpoints,end)./iNum(1:simpoints,:);
				bar3([simu_CPI, TOCS09_CPI, ISPASS11_CPI], .8, 'detached');
				colormap([1 0 0;0 1 0;0 0 1]);
				
                %figure(); plot(sMats.i(:,end)./iNum); hold on; plot(sMats.i(:,7)./iNum,'r');
                %figure(); bar(sMats.i(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10), 'stacked');
                figure(); bar(sMats.i(1:simpoints,end-10:end-1)./repmat(iNum(1:simpoints,:),1,10), 'stacked');
                legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
                    'MissPrediction','Idle','Serial','Conflicts');
                corrCoef = corrcoef(sMats.i(:,end), sMats.i(:,7))
            case 'mech+ANN'
                if(~exist('bpData.mat', 'file'))
                    sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,(colM+colI+3)) ...
                        +sMats.i(:,10)+sMats.i(:,(colM+colI+2))/MLP);       % Base2部分：dispatch低效因子
                    sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem/MLP; % L2ICache
                    sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem/MLP; % L2DCache
                    sMats.i(:,end+1) = sMats.i(:,10).*(pCDR+pFEDp);         % 分支预测失败开销
                    sMats.i(:,end+1) = sMats.i(:,colM+35);                  % 串行指令数目
                    sMats.i(:,end+1) = sum(sMats.i(:,24:26),2);             % 冲突开销
                    sMats.i(:,end+1) = sMats.i(:,8);                        % idle开销
                    sMats.i(:,end+1) = sum(sMats.i(:,end-10:end), 2);       % 计算总Cycle数目
                    figure(); plot(sMats.i(:,end)./iNum); hold on; plot(sMats.i(:,7)./iNum,'r');
                    corrCoef_Mech = corrcoef(sMats.i(:,end)./iNum, sMats.i(:,7)./iNum)
                    
                    adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust);
                    P=sMats.i(adjust,end-11:end-1)./repmat(iNum(adjust,:),1,11); % 最后一项是累加和，不需要
                    T=sMats.i(adjust,7)./iNum(adjust,:);
                    [~, Pcol] = size(P);
                    m=max(max(P)); n=max(max(T));
                    P=P'/m; T=T'/n; 					% 归一化
                    pr(1:Pcol,1)=0; pr(1:Pcol,2)=1; 	% 输入矢量范围
                    warning off all						% 关闭控制台上waring的提示
                    bpnet=newff(pr,[1 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
                    bpnet.trainParam.epochs=10000;   	% 允许最大训练步数2000步
                    bpnet.trainParam.goal=1e-5;     	% 训练目标最小误差0.001
                    bpnet.trainParam.show=100;      	% 每间隔100步显示一次训练结果
                    bpnet.trainParam.lr=0.05;       	% 学习速率0.05
                    %bpnet.trainParam.showWindow = false;% 关闭训练过程的GUI
                    [bpnet]=train(bpnet,P,T);       	% 开始训练网络
                    bpnet.iw{1,1}						% 隐藏层权重值
                    bpnet.b{1}							% 隐藏层阈值
                    bpnet.iw{2,1}						% 输出层权重值
                    bpnet.b{2}							% 输出层阈值
                    save('bpData.mat','bpnet','P','T','n','adjust'); % 保存
                else
                    load('bpData.mat');
                    R=(sim(bpnet,P))'*n;				% 仿真网络
                    figure(); plot(R,'g'); hold on; plot(sMats.i(adjust,7)./iNum(adjust,:),'r');
					WScope_CPI = R(simpoints:simpoints+points,:);
					colormap([0 0 1; 0 1 0; 0 1 1; 1 0 0]);
                    corrCoef_MechANN = corrcoef(R, sMats.i(adjust,7)./iNum(adjust,:))
	                figure(); bar(sMats.i(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10), 'stacked');
					figure(); bar(sMats.i(simpoints:simpoints+points,end-10:end-1)./repmat(iNum(simpoints:simpoints+points,:),1,10), 'stacked');
	                legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
	                    'MissPrediction','Idle','Serial','Conflicts');
                end
            case 'pureANN'
                P=[sMats.i(:,2),sMats.i(:,10),sMats.i(:,12),sMats.i(:,14) ...
                    sMats.i(:,(colM+colI+3)),sMats.i(:,(colM+colI+4))];
                adjust=find(sMats.i(:,2) < mean(sMats.i(:,2))*pAdjust);
                P=P(adjust,:)./repmat(iNum(adjust,:),1,6);
                T=sMats.i(adjust,7)./iNum(adjust,:);
                
                [~, Pcol] = size(P);
                m=max(max(P)); n=max(max(T));
                P=P'/m; T=T'/n;                 % 归一化
                pr(1:Pcol,1)=0; pr(1:Pcol,2)=1; % 输入矢量范围
                warning off all					% 关闭控制台上waring的提示
                bpnet=newff(pr,[20 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
                bpnet.trainParam.epochs=1000;   % 允许最大训练步数2000步
                bpnet.trainParam.goal=1e-5;     % 训练目标最小误差0.001
                bpnet.trainParam.show=100;      % 每间隔100步显示一次训练结果
                bpnet.trainParam.lr=0.05;       % 学习速率0.05
                bpnet.trainParam.showWindow = false;
                [bpnet]=train(bpnet,P,T);    	% 开始训练网络
                r=sim(bpnet,P); R=r'*n;
                figure(); plot(R); hold on; plot(sMats.i(adjust,7)./iNum(adjust,:),'r');
                corrCoef_MechANN = corrcoef(R, sMats.i(adjust,7)./iNum(adjust,:))
			case 'paper'
				%%%%%% TOCS09 part: Mechanic Model%%%%%%%%%%%
                sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd .*((sMats.i(:,2)) + ...
                    sMats.i(:, (colM+colI+3)) + sMats.i(:,10) + ...
                    sMats.i(:, (colM+colI+2))/MLP);                     % Base2：dispatch低效因子
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem/MLP; % L2ICache
                sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem/MLP; % L2DCache
                sMats.i(:,end+1) = sMats.i(:, 10).*(pCDR+pFEDp);        % 分支预测失败开销
                sMats.i(:,end+1) = sum(sMats.i(:,end-7:end),2);        % 总的Cycle数目
                
                adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust); % 去掉cycle数目过大的点
				TOCS09_CPI = sMats.i(simpoints:simpoints+points,end)./iNum(simpoints:simpoints+points,:);
				simu_CPI = sMats.i(simpoints:simpoints+points,7)./iNum(simpoints:simpoints+points,:);
				sMats.i(:,end-4:end) = [];
				
                % pCDR = b1 * (max(40, 1/mpubr)).^b2 * (1+b3*fp) * (1+b4*mpuDL1)
                pCDR = ['b(1).*(max(40,1/(sMats.i(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)*' ...
                    '(1+b(4).*sMats.i(:,colM+colI+1)./iNum)'];
                % MLP = b5 * (mpuDL2).^b6 * (mpuDTLB).^b7
                MLP = '(b(5).*((sMats.i(:,colM+colI+4)./iNum).^b(6)).*((sMats.i(:,14)./iNum).^b(7)))';
                % cStall = max(0, 1-cMiss./(iNum/D)+b8*(1+b9*fp)*(1+b10*mpuDL1))
                cM = '100';
                cStall_t = '(b(8).*(1+b(9).*iNumFP./iNum).*(1+b(10).*(sMats.i(:,colM+colI+1)./iNum)))';
                cStall =strcat('(max(0,1-',cM,'./(iNum/pFEDp+',cStall_t,')).*',cStall_t,')');
                
                if(~exist('ispass11-b.mat','file'))
                	% 为了避免多个文件，我这里用匿名的函数指针方式做拟合
               		beta0 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]'; % 初始化拟合权重
                	pDispatch=['(pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,(colM+colI+3))'...
                    	'+sMats.i(:,10)+sMats.i(:, (colM+colI+2))./']; % MLP); % dispatch低效因子
                	pL2I = 'sMats.i(:,(colM+colI+3)).*avgMem./'; % MLP;
                	pL2D = 'sMats.i(:,(colM+colI+4)).*avgMem./'; % MLP;
               		pBr = 'sMats.i(:,10).*(pFEDp+'; %pCDR);
               		eval(['[b,r,j,covb,mse] = nlinfit(sMats.i,sMats.i(:,7), @(b,m)', ...
                   		pDispatch,MLP,')+',pL2I,MLP,'+',pL2D,MLP,'+',pBr,pCDR,'+',cStall,')',',beta0);']);
               		b; % 显示拟合得到的b1, b2, b3, b4, b5, b6以及b7参数
					save('ispass11-b.mat', 'b');
				else
					load('ispass11-b.mat', 'b');
				end
                % 利用拟合出来的参数，重新计算得到CPI的值
                MLP = b(5).*((sMats.i(:,colM+colI+4)./iNum).^b(6)).*((sMats.i(:,14)./iNum).^b(7));
                pCDR = b(1).*(max(40,1/(sMats.i(:,10)./iNum))).^b(2)*(1+b(3).*iNumFP./iNum)* ...
                    (1+b(4).*sMats.i(:,colM+colI+1)./iNum);
                
                sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,  ...
                    (colM+colI+3))+sMats.i(:,10)+sMats.i(:, (colM+colI+2))./MLP); % dispatch低效因子
                sMats.i(:,end+1) = sMats.i(:,(colM + colI + 3)) * avgMem./MLP;    % L2ICache
                sMats.i(:,end+1) = sMats.i(:,(colM + colI + 4)) * avgMem./MLP;    % L2DCache
                sMats.i(:,end+1) = sMats.i(:,10) .* (pCDR + pFEDp);               % 分支预测失败开销
                sMats.i(:,end+1) = eval(['(max(0,1-',cM,'./(iNum/pFEDp+', ...
               		cStall_t,')).*',cStall_t,')']);								  % stall开销
                sMats.i(:,end+1) = sum(sMats.i(:, end-5: end), 2);                % 总的Cycle数目
                
                adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust); 		  % 去掉cycle数目过大的点
				
				ISPASS11_CPI = sMats.i(simpoints:simpoints+points,end)./iNum(simpoints:simpoints+points,:);
				sMats.i(:,end-5:end) = [];
				
				%%%%%%%%%%%%%%%%%%%%%%%%%
				MLP = 3; pCDR = 5;
                if(~exist('bpData.mat', 'file'))
                    sMats.i(:,end+1) = (pFEWd-1)/2/pFEWd.*((sMats.i(:,2))+sMats.i(:,(colM+colI+3)) ...
                        +sMats.i(:,10)+sMats.i(:,(colM+colI+2))/MLP);       % Base2部分：dispatch低效因子 +1
                    sMats.i(:,end+1) = sMats.i(:,(colM+colI+3))*avgMem/MLP; % L2ICache +2
                    sMats.i(:,end+1) = sMats.i(:,(colM+colI+4))*avgMem/MLP; % L2DCache +3
                    sMats.i(:,end+1) = sMats.i(:,10).*(pCDR+pFEDp);         % 分支预测失败开销  +4
                    sMats.i(:,end+1) = sMats.i(:,colM+35);                  % 串行指令数目     +5
                    sMats.i(:,end+1) = sum(sMats.i(:,24:26),2);             % 冲突开销        +6
                    sMats.i(:,end+1) = sMats.i(:,8);                        % idle开销       +7
                    sMats.i(:,end+1) = sum(sMats.i(:,end-10:end), 2);       % 计算总Cycle数目 +8
                    figure(); plot(sMats.i(:,end)./iNum); hold on; plot(sMats.i(:,7)./iNum,'r');
                    corrCoef_Mech = corrcoef(sMats.i(:,end)./iNum, sMats.i(:,7)./iNum)
                    
                    adjust=find(sMats.i(:,7) < mean(sMats.i(:,7))*pAdjust);
                    P=sMats.i(adjust,end-11:end-1)./repmat(iNum(adjust,:),1,11); % 最后一项是累加和，不需要
                    T=sMats.i(adjust,7)./iNum(adjust,:);
                    [~, Pcol] = size(P);
                    m=max(max(P)); n=max(max(T));
                    P=P'/m; T=T'/n; 					% 归一化
                    pr(1:Pcol,1)=0; pr(1:Pcol,2)=1; 	% 输入矢量范围
                    warning off all						% 关闭控制台上waring的提示
                    bpnet=newff(pr,[1 1],{'purelin', 'purelin'}, 'traingdx', 'trainbfg');
                    bpnet.trainParam.epochs=10000;   	% 允许最大训练步数2000步
                    bpnet.trainParam.goal=1e-5;     	% 训练目标最小误差0.001
                    bpnet.trainParam.show=100;      	% 每间隔100步显示一次训练结果
                    bpnet.trainParam.lr=0.05;       	% 学习速率0.05
                    %bpnet.trainParam.showWindow = false;% 关闭训练过程的GUI
                    [bpnet]=train(bpnet,P,T);       	% 开始训练网络
                    bpnet.iw{1,1}						% 隐藏层权重值
                    bpnet.b{1}							% 隐藏层阈值
                    bpnet.iw{2,1}						% 输出层权重值
                    bpnet.b{2}							% 输出层阈值
                    save('bpData.mat','bpnet','P','T','n','adjust'); % 保存
                else
                    load('bpData.mat');
                    R=(sim(bpnet,P))'*n;				% 仿真网络
                    %figure(); plot(R,'g'); hold on; plot(sMats.i(adjust,7)./iNum(adjust,:),'r');
					WScope_CPI = R(simpoints:simpoints+points,:);
					%bar3([simu_CPI, WScope_CPI, TOCS09_CPI, ISPASS11_CPI], 'detached');
					%bar3([simu_CPI, WScope_CPI, ISPASS11_CPI], 'detached');

					results_CPI = [simu_CPI, ISPASS11_CPI, TOCS09_CPI, WScope_CPI];
					for ii = 1:points
						if isnan(results_CPI(ii, 2))
							fprintf('F')
							results_CPI(ii,2) = (results_CPI(ii, 1)+ results_CPI(ii, 3))/2;
						end
					end
					simu_CPI = results_CPI(:,1);
					ISPASS11_CPI = results_CPI(:,2);
					TOCS09_CPI = results_CPI(:,3);
					WScope_CPI = results_CPI(:,4);
					plot(simu_CPI, 'r-*', 'LineWidth', 1.0);
					hold all;
					plot(ISPASS11_CPI, 'g-+', 'LineWidth', 1.0);
					hold all;
					plot(TOCS09_CPI, 'b-o', 'LineWidth', 1.0);
					hold all;
					plot(WScope_CPI, 'k-square',  'LineWidth', 1.0);
					legend('GEM5 SIMULATION', 'MECH+REGRESSION', 'MECHANICS', 'OURS');
					csvwrite('results_CPI.csv', results_CPI);
                    corrCoef_MechANN = corrcoef(R, sMats.i(adjust,7)./iNum(adjust,:))
	                %figure(); bar(sMats.i(adjust,end-10:end-1)./repmat(iNum(adjust,:),1,10), 'stacked');
					figure(); bar(sMats.i(simpoints:simpoints+points, end-10:end-1)./repmat(iNum(simpoints:simpoints+points,:),1,10), 'stacked');
	                legend('Base1','L1ICache Miss','L1ITLB','L1DTLB','dispatch','L2ICache','L2DCache', ...
	                    'MissPrediction','Idle','Serial','Conflicts');
                end
            otherwise
                fprintf('Unkonwn Model!\n');
        end
    end
end
etime(clock, t1)

end
% EOF




